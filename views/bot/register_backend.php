<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Bot */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bot-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Done', ['index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
    <table class="table table-striped table-bordered detail-view">
        <?php foreach($response as $key => $val): ?>
        <tr>
            <th>
                <?= $key ?>: 
            </th>
            <td>
                <?= var_export($val, true) ?>
            </td>
        </tr>
        <?php endforeach ?>
    <table>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Bot',
                'attribute' => 'name_bot',
                'value' => fn($data) => '@' . $data->name_bot,
            ],            
            'name',
            'backend',
            'token',
            [
                'attribute' => 'active',
                'value' => fn($data) => var_export((boolean)$data->active, true),
            ],            
        ],
    ]) ?>

</div>
