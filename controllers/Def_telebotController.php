<?php
namespace app\controllers;
error_reporting(E_ALL);


use Yii;
use app\models\Bot;
use app\models\Talk;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use Telegram\Bot\Api; 

/**
 * BotController implements the CRUD actions for Bot model.
 */
class Def_telebotController extends Controller
{

    protected $telegram;
    protected $user_action;
    protected $message;
    protected $answer;
    protected $talk;

    public function behaviors()
    {
        return [
            'verbs' => [
                    'class' => VerbFilter::className(),
                'actions' => [
                    'hook' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }
    
    public function actionIndex()
    {
        return; //$this->render('index');
    }
    
    protected function initModel(string $model_name, array $fattr = [], array $attr = [])
    {
        $model = false;
        if (!($model = $model_name::find()->where($fattr)->one()))
        {
            $model = new $model_name;
            foreach ($attr as $key => $val) $model->$key = $val;
            $model->save();
        }
        return $model;
    }
    
    function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $name_bot = strtolower(str_replace(
            'Controller',
            '',
            \yii\helpers\StringHelper::basename(get_class($this))
        )) . '_bot';

        if (!($botModel = Bot::find()->where(['name_bot' => $name_bot])->one())) return false;
        $this->talk = new Talk;
        $this->talk->bot_id = $botModel->id;
        $this->talk->iscommand = 0;
        $this->telegram = new Api($botModel->token);
        $this->user_action = $this->telegram -> getWebhookUpdates(); 
        if ( isset($this->user_action["edited_message"]) or isset($this->user_action["message"]) ) 
        {
            $m ="message";
            if ( isset($this->user_action["edited_message"]) ) $m = "edited_message";
            $this->message = $this->user_action[$m];
            $this->talk->update_id = $this->user_action['update_id'];
            $this->talk->chat_id = $this->message['chat']['id'];
            $this->talk->user_id = $this->initModel('\app\models\User', [
                'id' => $this->message['from']['id'],
            ],
            [
                'id' => $this->message['from']['id'],
                'username' => $this->message['from']['username'],
                'first_name' => $this->message['from']['first_name'],
                'language_code' => $this->message['from']['language_code'],
                'is_bot' => (int)$this->message['from']['is_bot'],
            ])->id;
        }
    }

    public function __destruct()
    {
        if (isset($this->message["text"]))
        {
            $this->talk->message_type_id = 
                $this->initModel('\app\models\MessageType', 
                ['action' => $this->message["text"]], 
                ['action' => $this->message["text"]])->id;
        }
    
        $this->talk->answer_id = $this->initModel('\app\models\Answer', ['answer' => $this->answer], ['answer' => $this->answer])->id;
        if ($this->answer)
            $this->talk->success =         
                $this->telegram->sendMessage([
//                     'chat_id' => $this->message['chat']['id'],
                    'chat_id' => $this->talk->user_id,
                    'parse_mode' => 'html',
                    'text' => $this->answer,
                ]) ? 1:0;
        $this->talk->save();
    }
    
    public function actionHook()
    {
        $this->answer = "Hello, "
        . $this->message['from']['first_name']
        . "!\nYou say '" 
        . $this->message["text"]
        . "',\n Allright!\n";        
    }

}
