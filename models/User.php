<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $first_name
 * @property string $language_code
 * @property int $is_bot
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'username', 'first_name', 'language_code'], 'required'],
            [['id', 'is_bot'], 'integer'],
            [['username', 'first_name'], 'string', 'max' => 64],
            [['language_code'], 'string', 'max' => 2],
            [['username'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'first_name' => 'First Name',
            'language_code' => 'Language Code',
            'is_bot' => 'Is Bot',
        ];
    }
}
