<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FavCurr */

$this->title = 'Create Fav Curr';
$this->params['breadcrumbs'][] = ['label' => 'Fav Currs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fav-curr-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
