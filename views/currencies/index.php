<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Currencies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bot-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//             ['class' => 'yii\grid\SerialColumn'],

            'id',
            'src',
            'dst',
            'value',
            'updated',
//             ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
