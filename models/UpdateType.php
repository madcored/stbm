<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "update_type".
 *
 * @property int $id
 * @property string $update
 */
class UpdateType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'update_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['update'], 'required'],
            [['update'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'update' => 'Update',
        ];
    }
}
