<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IndexViewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="index-view-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date_time') ?>

    <?= $form->field($model, 'Bot') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'command') ?>

    <?php // echo $form->field($model, 'iscommand') ?>

    <?php // echo $form->field($model, 'answere') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
