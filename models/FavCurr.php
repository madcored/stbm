<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fav_curr".
 *
 * @property int $id
 * @property int $user_id
 * @property string $src
 * @property string $dst
 *
 * @property User $user
 */
class FavCurr extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fav_curr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'dst'], 'required'],
            [['id', 'user_id'], 'integer'],
            [['src', 'dst'], 'string', 'max' => 3],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'src' => 'Src',
            'dst' => 'Dst',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
