<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bot".
 *
 * @property int $id
 * @property string $name_bot
 * @property string $name
 * @property string $backend
 * @property string $token
 * @property int $active
 *
 * @property Actions[] $actions
 */
class Bot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_bot', 'name', 'backend', 'token'], 'required'],
            [['active'], 'boolean'],
            [['active'], 'default', 'value' => 0],
            [['name_bot', 'name', 'token'], 'string', 'max' => 64],
            [['backend'], 'string', 'max' => 128],
            [['name_bot'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_bot' => 'Name Bot',
            'name' => 'Name',
            'backend' => 'Backend',
            'token' => 'Token',
            'active' => 'Active',
        ];
    }

    /**
     * Gets query for [[Actions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActions()
    {
        return $this->hasMany(Actions::className(), ['bot_id' => 'id']);
    }

    public function getByName($name)
    {
        return $this->hasMany(Actions::className(), ['bot_id' => 'id']);
    }

}
