<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "talk".
 *
 * @property int $id
 * @property string $date
 * @property int $bot_id
 * @property int $user_id
 * @property int $message_type_id
 * @property int $answer_id
 * @property int $update_id
 *
 * @property User $user
 * @property Bot $bot
 * @property MessageType $messageType
 * @property Answer $answer
 */
class Talk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'talk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bot_id', 'user_id', 'message_type_id', 'answer_id', 'update_id'], 'required'],
            [['id', 'bot_id', 'user_id', 'message_type_id', 'answer_id', 'update_id'], 'integer'],
            [['date_time'], 'safe'],
            [['id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
            [['message_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MessageType::className(), 'targetAttribute' => ['message_type_id' => 'id']],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answer::className(), 'targetAttribute' => ['answer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_time' => 'date_time',
            'bot_id' => 'Bot ID',
            'user_id' => 'User ID',
            'message_type_id' => 'Message Type ID',
            'answer_id' => 'Answer ID',
            'update_id' => 'Update ID',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Bot]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    /**
     * Gets query for [[MessageType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMessageType()
    {
        return $this->hasOne(MessageType::className(), ['id' => 'message_type_id']);
    }

    /**
     * Gets query for [[Answer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(Answer::className(), ['id' => 'answer_id']);
    }
}
