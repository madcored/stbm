<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndexViewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activity';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="index-view-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Manage Bots', ['/bot'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('View Subscriptions', ['/subscription'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('View Currencies', ['/currencies'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'date_time',
            'Bot',
            'username',
            'command',
            [
                'attribute' => 'iscommand',
                'contentOptions' => ['style' => 'min-width:90px;text-align:center;'],
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'iscommand',
                    [null => '---', 1 => 'Yes', 0 => 'No'],
                    ['class' => 'form-control']),
                    'value' => fn($data) =>  var_export((boolean)$data->iscommand, true),
            ],
            'answere:ntext',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
