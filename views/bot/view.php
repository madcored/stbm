<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Bot */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bot-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Done', ['index', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Register Hook', ['register', 'id' => $model->id ], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Remove Hook', ['unregister', 'id' => $model->id ], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Bot',
                'attribute' => 'name_bot',
                'value' => fn($data) => '@' . $data->name_bot,
            ],            
            'name',
            'backend',
            'token',
            [
                'attribute' => 'active',
                'value' => fn($data) => var_export((boolean)$data->active, true),
            ],            
        ],
    ]) ?>

</div>
