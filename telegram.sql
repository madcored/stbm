-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: telegram
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `answer` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `answer` (`answer`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=339 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (319,' <b>Welcome, m҉a҉d҉c҉o҉r҉e҉</b> \n I`m just a currency bot and know nothing.\n Loan /help for a variety.\n'),(320,'You can control me by sending these commands:\n\n /start 	 - For start \n /help 	 - For help \n /котик 	 - котик\n\n<b>Currencies info</b> \n /show 	 - View currencies \n /fav 	 - Show your favorit currencies \n /addfav 	 - Add currencies to your favorites \n /delfav 	 - Remove currencies from your favorites \n /all 	 - View all currencies\n\n<b>subscriptions</b> \n /sub 	 - View your scheduled subscriptions \n /addsub 	 - Add your favs to scheduled subscriptions \n /delsub 	 - Remove from scheduled subscriptions\n\n<b>History</b> \n /history 	 - View your history for date inretval \n /subhistory 	 - View your sheduled task history \n'),(321,'Enter currencies to view, space separated, for example:\nCNY JPY INR'),(322,'USD / CNY : 	6.3914 \nUSD / INR : 	72.813503 \nUSD / JPY : 	109.374992 \n'),(323,'USD / EUR : 	0.820501 \nUSD / GBP : 	0.70565 \nUSD / RUB : 	72.803102 \n'),(324,'USD / CNY : 	6.3914 \nUSD / EUR : 	0.820501 \nUSD / GBP : 	0.70565 \nUSD / INR : 	72.813503 \nUSD / JPY : 	109.374992 \nUSD / RUB : 	72.803102 \n'),(325,'Enter days of week with time, for example:\nmon 11:00 / fri 10:00, 17:50 / * 12:45 / workdays 10:00,12:00, 14:00, 16:00'),(326,'<b>Your sheduled subscriptions: </b>\nnothing'),(327,'USD / CNY : 	6.3914 \nUSD / EUR : 	0.820501 \nUSD / GBP : 	0.70565 \nUSD / JPY : 	109.374992 \nUSD / RUB : 	72.803102 \n'),(328,'USD / AED : 	3.673199 \nUSD / AFN : 	78.450216 \nUSD / ALL : 	101.190296 \nUSD / AMD : 	520.02977 \nUSD / ANG : 	1.794836 \nUSD / AOA : 	641.999571 \nUSD / ARS : 	94.897994 \nUSD / AUD : 	1.29003 \nUSD / AWG : 	1.8 \nUSD / AZN : 	1.694136 \nUSD / BAM : 	1.60881 \nUSD / BBD : 	2.018886 \nUSD / BDT : 	84.787655 \nUSD / BGN : 	1.602713 \nUSD / BHD : 	0.37702 \nUSD / BIF : 	1975 \nUSD / BMD : 	1 \nUSD / BND : 	1.323825 \nUSD / BOB : 	6.969425 \nUSD / BRL : 	5.046902 \nUSD / BSD : 	0.999901 \nUSD / BTC : 	0.000029575696 \nUSD / BTN : 	72.798095 \nUSD / BWP : 	10.547577 \nUSD / BYN : 	2.528584 \nUSD / BYR : 	19600 \nUSD / BZD : 	2.01553 \nUSD / CAD : 	1.208555 \nUSD / CDF : 	1998.999694 \nUSD / CHF : 	0.89739 \nUSD / CLF : 	0.025963 \nUSD / CLP : 	716.397004 \nUSD / CNY : 	6.3914 \nUSD / COP : 	3607.5 \nUSD / CRC : 	619.492975 \nUSD / CUC : 	1 \nUSD / CUP : 	26.5 \nUSD / CVE : 	90.799511 \nUSD / CZK : 	20.853402 \nUSD / DJF : 	177.720203 \nUSD / DKK : 	6.10195 \nUSD / DOP : 	57.040143 \nUSD / DZD : 	133.291497 \nUSD / EGP : 	15.6928 \nUSD / ERN : 	15.00199 \nUSD / ETB : 	43.150129 \nUSD / EUR : 	0.820501 \nUSD / FJD : 	2.028901 \nUSD / FKP : 	0.711515 \nUSD / GBP : 	0.70565 \nUSD / GEL : 	3.140415 \nUSD / GGP : 	0.711515 \nUSD / GHS : 	5.78008 \nUSD / GIP : 	0.711515 \nUSD / GMD : 	51.149654 \nUSD / GNF : 	9809.739245 \nUSD / GTQ : 	7.724229 \nUSD / GYD : 	208.709314 \nUSD / HKD : 	7.75842 \nUSD / HNL : 	24.105966 \nUSD / HRK : 	6.154605 \nUSD / HTG : 	92.615898 \nUSD / HUF : 	284.633993 \nUSD / IDR : 	14256.8 \nUSD / ILS : 	3.24639 \nUSD / IMP : 	0.711515 \nUSD / INR : 	72.813503 \nUSD / IQD : 	1460.5 \nUSD / IRR : 	42104.999994 \nUSD / ISK : 	120.359975 \nUSD / JEP : 	0.711515 \nUSD / JMD : 	148.337186 \nUSD / JOD : 	0.708996 \nUSD / JPY : 	109.374992 \nUSD / KES : 	107.900967 \nUSD / KGS : 	84.630897 \nUSD / KHR : 	4080.000147 \nUSD / KMF : 	404.150178 \nUSD / KPW : 	900.000118 \nUSD / KRW : 	1112.689596 \nUSD / KWD : 	0.300601 \nUSD / KYD : 	0.833251 \nUSD / KZT : 	427.210661 \nUSD / LAK : 	9449.999596 \nUSD / LBP : 	1527.497355 \nUSD / LKR : 	197.482952 \nUSD / LRD : 	171.250528 \nUSD / LSL : 	13.494979 \nUSD / LTL : 	2.95274 \nUSD / LVL : 	0.60489 \nUSD / LYD : 	4.450048 \nUSD / MAD : 	8.820289 \nUSD / MDL : 	17.693592 \nUSD / MGA : 	3753.000151 \nUSD / MKD : 	50.539103 \nUSD / MMK : 	1645.824049 \nUSD / MNT : 	2850.913096 \nUSD / MOP : 	7.989751 \nUSD / MRO : 	356.999828 \nUSD / MUR : 	40.648906 \nUSD / MVR : 	15.449957 \nUSD / MWK : 	800.0004 \nUSD / MXN : 	19.84127 \nUSD / MYR : 	4.11978 \nUSD / MZN : 	61.650185 \nUSD / NAD : 	13.494963 \nUSD / NGN : 	412.540215 \nUSD / NIO : 	35.339949 \nUSD / NOK : 	8.26755 \nUSD / NPR : 	116.477614 \nUSD / NZD : 	1.38437 \nUSD / OMR : 	0.384981 \nUSD / PAB : 	0.999901 \nUSD / PEN : 	3.842501 \nUSD / PGK : 	3.510318 \nUSD / PHP : 	47.673984 \nUSD / PKR : 	154.512314 \nUSD / PLN : 	3.66556 \nUSD / PYG : 	6759.918073 \nUSD / QAR : 	3.641027 \nUSD / RON : 	4.038985 \nUSD / RSD : 	96.722053 \nUSD / RUB : 	72.803102 \nUSD / RWF : 	990 \nUSD / SAR : 	3.751184 \nUSD / SBD : 	7.984064 \nUSD / SCR : 	16.500234 \nUSD / SDG : 	427.502564 \nUSD / SEK : 	8.26122 \nUSD / SGD : 	1.322535 \nUSD / SHP : 	0.711515 \nUSD / SLL : 	10234.999938 \nUSD / SOS : 	585.000513 \nUSD / SRD : 	14.153973 \nUSD / STD : 	20735.784508 \nUSD / SVC : 	8.749548 \nUSD / SYP : 	1257.535268 \nUSD / SZL : 	13.494958 \nUSD / THB : 	31.197992 \nUSD / TJS : 	11.404129 \nUSD / TMT : 	3.51 \nUSD / TND : 	2.726497 \nUSD / TOP : 	2.23145 \nUSD / TRY : 	8.613597 \nUSD / TTD : 	6.78273 \nUSD / TWD : 	27.7005 \nUSD / TZS : 	2318.999768 \nUSD / UAH : 	27.175372 \nUSD / UGX : 	3537.685797 \nUSD / USD : 	1 \nUSD / UYU : 	43.590988 \nUSD / UZS : 	10547.000288 \nUSD / VEF : 	213830222338.07 \nUSD / VND : 	22955.5 \nUSD / VUV : 	108.407216 \nUSD / WST : 	2.51595 \nUSD / XAF : 	539.562564 \nUSD / XAG : 	0.035917 \nUSD / XAU : 	0.000527 \nUSD / XCD : 	2.70255 \nUSD / XDR : 	0.694979 \nUSD / XOF : 	541.498708 \nUSD / XPF : 	98.450127 \nUSD / YER : 	250.049784 \nUSD / ZAR : 	13.53317 \nUSD / ZMK : 	9001.200677 \nUSD / ZMW : 	22.543041 \nUSD / ZWL : 	322.000305 \n'),(329,'nothing'),(330,'<b>Your sheduled subscriptions: </b>\n\n<b> wed:</b>\n09:05:00\n'),(331,'You cute, m҉a҉d҉c҉o҉r҉e҉\n Try /help for more info'),(332,'<b>Your sheduled subscriptions: </b>\n\n<b> wed:</b>\n09:05:00\n09:15:00\n\n<b> wed thu:</b>\n09:10:00\n'),(333,'<b>Your sheduled subscriptions: </b>\n\n<b> wed:</b>\n09:05:00\n09:15:00\n\n<b> wed thu:</b>\n09:10:00\n\n<b> work days(mon - fri):</b>\n10:00:00\n'),(334,'Enter date interval \'Y/m/d - Y/m/d\', for example:\n2021/06/03 - 2021/06/08'),(335,'\n\n<b>DONE</b>'),(336,'котик'),(337,'<b>Your sheduled subscriptions: </b>\n\n<b> wed:</b>\n09:05:00\n09:15:00\n\n<b> wed thu:</b>\n09:10:00\n\n<b> work days(mon - fri):</b>\n10:00:00\n\n<b> weekends(sat - mon):</b>\n12:20:00\n'),(338,'\n<b> wed:</b>\n09:05:00\n09:15:00\n\n<b> wed thu:</b>\n09:10:00\n\n<b> work days(mon - fri):</b>\n10:00:00\n\n<b> weekends(sat - sun):</b>\n12:20:00\n');
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bot`
--

DROP TABLE IF EXISTS `bot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bot` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name_bot` varchar(64) CHARACTER SET ascii NOT NULL,
  `name` varchar(64) NOT NULL,
  `backend` varchar(128) NOT NULL,
  `token` varchar(64) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_bot` (`name_bot`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='Bots list';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bot`
--

LOCK TABLES `bot` WRITE;
/*!40000 ALTER TABLE `bot` DISABLE KEYS */;
INSERT INTO `bot` VALUES (1,'zoltor_curr_bot','Currencies Bot','https://7b13da6192e6.ngrok.io/zoltor_curr/hook','1749549106:AAHRTyOk3BhtFEXPUfUUJ1K_2AhWwbTGYlY',1),(2,'madcore_bot','madcore','https://7b13da6192e6.ngrok.io/madcore/hook','1832228313:AAFWbL0LnzGs-N_WVd_EEaYxHKJAaLbO7EE',0),(3,'zoltor24-test_bot','zoltor24-test','https://e6175481198f.ngrok.io/telegram_bot.php','1735200468:AAEipGhuZiZ9k-lkhie0qrV-SUlswxx-IkA',0),(12,'def_telebot_bot','Test TeleBot','https://7b13da6192e6.ngrok.io/def_telebot/hook','1722750274:AAH_sSWLiNOJVnHBQjZhoLBXApiIMX8UKpk',1),(13,'jat777_bot','Just another test','https://7b13da6192e6.ngrok.io/jat777/hook','1895053453:AAHZZoPbAqEN3INdhe2KzbjLD8EIf5zCKIk',1),(15,'vasia6666_bot','Вася666','https://e6175481198f.ngrok.io/telegram_bot2.php','1868791251:AAHpIt1XJHsq8V1h82_r9WCbHmZJmQlStBg',0);
/*!40000 ALTER TABLE `bot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(3) CHARACTER SET ascii NOT NULL,
  `dst` varchar(3) CHARACTER SET ascii NOT NULL,
  `value` double unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `pair` (`src`,`dst`),
  KEY `dst` (`dst`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` VALUES (1,'USD','AED',3.673199,'2021-06-08 18:21:01'),(2,'USD','AFN',78.450216,'2021-06-08 18:21:01'),(3,'USD','ALL',101.190296,'2021-06-08 18:21:01'),(4,'USD','AMD',520.02977,'2021-06-08 18:21:01'),(5,'USD','ANG',1.794836,'2021-06-08 18:21:01'),(6,'USD','AOA',641.999571,'2021-06-08 18:21:01'),(7,'USD','ARS',94.897994,'2021-06-08 18:21:01'),(8,'USD','AUD',1.29003,'2021-06-08 18:21:01'),(9,'USD','AWG',1.8,'2021-06-05 02:27:57'),(10,'USD','AZN',1.694136,'2021-06-08 18:21:01'),(11,'USD','BAM',1.60881,'2021-06-08 18:21:01'),(12,'USD','BBD',2.018886,'2021-06-08 18:21:01'),(13,'USD','BDT',84.787655,'2021-06-08 18:21:01'),(14,'USD','BGN',1.602713,'2021-06-08 18:21:01'),(15,'USD','BHD',0.37702,'2021-06-08 18:21:01'),(16,'USD','BIF',1975,'2021-06-05 02:27:57'),(17,'USD','BMD',1,'2021-06-04 05:18:13'),(18,'USD','BND',1.323825,'2021-06-08 18:21:01'),(19,'USD','BOB',6.969425,'2021-06-08 18:21:01'),(20,'USD','BRL',5.046902,'2021-06-08 18:21:01'),(21,'USD','BSD',0.999901,'2021-06-08 18:21:01'),(22,'USD','BTC',0.000029575696,'2021-06-08 18:21:01'),(23,'USD','BTN',72.798095,'2021-06-08 18:21:01'),(24,'USD','BWP',10.547577,'2021-06-08 18:21:01'),(25,'USD','BYN',2.528584,'2021-06-08 18:21:01'),(26,'USD','BYR',19600,'2021-06-04 05:18:13'),(27,'USD','BZD',2.01553,'2021-06-08 18:21:01'),(28,'USD','CAD',1.208555,'2021-06-08 18:21:01'),(29,'USD','CDF',1998.999694,'2021-06-08 18:21:01'),(30,'USD','CHF',0.89739,'2021-06-08 18:21:01'),(31,'USD','CLF',0.025963,'2021-06-08 18:21:01'),(32,'USD','CLP',716.397004,'2021-06-08 18:21:01'),(33,'USD','CNY',6.3914,'2021-06-08 18:21:01'),(34,'USD','COP',3607.5,'2021-06-05 02:27:57'),(35,'USD','CRC',619.492975,'2021-06-08 18:21:01'),(36,'USD','CUC',1,'2021-06-04 05:18:13'),(37,'USD','CUP',26.5,'2021-06-04 05:18:13'),(38,'USD','CVE',90.799511,'2021-06-08 18:21:01'),(39,'USD','CZK',20.853402,'2021-06-08 18:21:01'),(40,'USD','DJF',177.720203,'2021-06-08 18:21:01'),(41,'USD','DKK',6.10195,'2021-06-08 18:21:01'),(42,'USD','DOP',57.040143,'2021-06-08 18:21:01'),(43,'USD','DZD',133.291497,'2021-06-08 18:21:01'),(44,'USD','EGP',15.6928,'2021-06-08 18:21:01'),(45,'USD','ERN',15.00199,'2021-06-04 05:18:13'),(46,'USD','ETB',43.150129,'2021-06-08 18:21:01'),(47,'USD','EUR',0.820501,'2021-06-08 18:21:01'),(48,'USD','FJD',2.028901,'2021-06-08 18:21:01'),(49,'USD','FKP',0.711515,'2021-06-04 05:18:13'),(50,'USD','GBP',0.70565,'2021-06-08 18:21:01'),(51,'USD','GEL',3.140415,'2021-06-08 18:21:01'),(52,'USD','GGP',0.711515,'2021-06-04 05:18:13'),(53,'USD','GHS',5.78008,'2021-06-08 18:21:01'),(54,'USD','GIP',0.711515,'2021-06-04 05:18:13'),(55,'USD','GMD',51.149654,'2021-06-08 18:21:01'),(56,'USD','GNF',9809.739245,'2021-06-08 18:21:01'),(57,'USD','GTQ',7.724229,'2021-06-08 18:21:01'),(58,'USD','GYD',208.709314,'2021-06-08 18:21:01'),(59,'USD','HKD',7.75842,'2021-06-08 18:21:01'),(60,'USD','HNL',24.105966,'2021-06-08 18:21:01'),(61,'USD','HRK',6.154605,'2021-06-08 18:21:01'),(62,'USD','HTG',92.615898,'2021-06-08 18:21:01'),(63,'USD','HUF',284.633993,'2021-06-08 18:21:01'),(64,'USD','IDR',14256.8,'2021-06-08 18:21:01'),(65,'USD','ILS',3.24639,'2021-06-08 18:21:01'),(66,'USD','IMP',0.711515,'2021-06-04 05:18:13'),(67,'USD','INR',72.813503,'2021-06-08 18:21:01'),(68,'USD','IQD',1460.5,'2021-06-05 02:27:57'),(69,'USD','IRR',42104.999994,'2021-06-08 18:21:01'),(70,'USD','ISK',120.359975,'2021-06-08 18:21:01'),(71,'USD','JEP',0.711515,'2021-06-04 05:18:13'),(72,'USD','JMD',148.337186,'2021-06-08 18:21:01'),(73,'USD','JOD',0.708996,'2021-06-08 18:21:01'),(74,'USD','JPY',109.374992,'2021-06-08 18:21:01'),(75,'USD','KES',107.900967,'2021-06-08 18:21:01'),(76,'USD','KGS',84.630897,'2021-06-08 18:21:01'),(77,'USD','KHR',4080.000147,'2021-06-08 18:21:01'),(78,'USD','KMF',404.150178,'2021-06-08 18:21:01'),(79,'USD','KPW',900.000118,'2021-06-04 05:18:13'),(80,'USD','KRW',1112.689596,'2021-06-08 18:21:01'),(81,'USD','KWD',0.300601,'2021-06-08 18:21:01'),(82,'USD','KYD',0.833251,'2021-06-08 18:21:01'),(83,'USD','KZT',427.210661,'2021-06-08 18:21:01'),(84,'USD','LAK',9449.999596,'2021-06-08 18:21:01'),(85,'USD','LBP',1527.497355,'2021-06-08 18:21:01'),(86,'USD','LKR',197.482952,'2021-06-08 18:21:01'),(87,'USD','LRD',171.250528,'2021-06-08 18:21:01'),(88,'USD','LSL',13.494979,'2021-06-08 18:21:01'),(89,'USD','LTL',2.95274,'2021-06-04 05:18:13'),(90,'USD','LVL',0.60489,'2021-06-04 05:18:13'),(91,'USD','LYD',4.450048,'2021-06-08 18:21:01'),(92,'USD','MAD',8.820289,'2021-06-08 18:21:01'),(93,'USD','MDL',17.693592,'2021-06-08 18:21:01'),(94,'USD','MGA',3753.000151,'2021-06-08 18:21:01'),(95,'USD','MKD',50.539103,'2021-06-08 18:21:01'),(96,'USD','MMK',1645.824049,'2021-06-08 18:21:01'),(97,'USD','MNT',2850.913096,'2021-06-04 05:18:13'),(98,'USD','MOP',7.989751,'2021-06-08 18:21:01'),(99,'USD','MRO',356.999828,'2021-06-04 05:18:13'),(100,'USD','MUR',40.648906,'2021-06-08 18:21:01'),(101,'USD','MVR',15.449957,'2021-06-08 18:21:01'),(102,'USD','MWK',800.0004,'2021-06-08 18:21:01'),(103,'USD','MXN',19.84127,'2021-06-08 18:21:01'),(104,'USD','MYR',4.11978,'2021-06-08 18:21:01'),(105,'USD','MZN',61.650185,'2021-06-08 18:21:01'),(106,'USD','NAD',13.494963,'2021-06-08 18:21:01'),(107,'USD','NGN',412.540215,'2021-06-08 18:21:01'),(108,'USD','NIO',35.339949,'2021-06-08 18:21:01'),(109,'USD','NOK',8.26755,'2021-06-08 18:21:01'),(110,'USD','NPR',116.477614,'2021-06-08 18:21:01'),(111,'USD','NZD',1.38437,'2021-06-08 18:21:01'),(112,'USD','OMR',0.384981,'2021-06-08 18:21:01'),(113,'USD','PAB',0.999901,'2021-06-08 18:21:01'),(114,'USD','PEN',3.842501,'2021-06-08 18:21:01'),(115,'USD','PGK',3.510318,'2021-06-08 18:21:01'),(116,'USD','PHP',47.673984,'2021-06-08 18:21:01'),(117,'USD','PKR',154.512314,'2021-06-08 18:21:01'),(118,'USD','PLN',3.66556,'2021-06-08 18:21:01'),(119,'USD','PYG',6759.918073,'2021-06-08 18:21:01'),(120,'USD','QAR',3.641027,'2021-06-08 18:21:01'),(121,'USD','RON',4.038985,'2021-06-08 18:21:01'),(122,'USD','RSD',96.722053,'2021-06-08 18:21:01'),(123,'USD','RUB',72.803102,'2021-06-08 18:21:01'),(124,'USD','RWF',990,'2021-06-05 02:27:57'),(125,'USD','SAR',3.751184,'2021-06-08 18:21:01'),(126,'USD','SBD',7.984064,'2021-06-05 02:27:57'),(127,'USD','SCR',16.500234,'2021-06-08 18:21:01'),(128,'USD','SDG',427.502564,'2021-06-08 18:21:01'),(129,'USD','SEK',8.26122,'2021-06-08 18:21:01'),(130,'USD','SGD',1.322535,'2021-06-08 18:21:01'),(131,'USD','SHP',0.711515,'2021-06-04 05:18:13'),(132,'USD','SLL',10234.999938,'2021-06-08 18:21:01'),(133,'USD','SOS',585.000513,'2021-06-08 18:21:01'),(134,'USD','SRD',14.153973,'2021-06-08 18:21:01'),(135,'USD','STD',20735.784508,'2021-06-04 05:18:13'),(136,'USD','SVC',8.749548,'2021-06-08 18:21:01'),(137,'USD','SYP',1257.535268,'2021-06-04 05:18:13'),(138,'USD','SZL',13.494958,'2021-06-08 18:21:01'),(139,'USD','THB',31.197992,'2021-06-08 18:21:01'),(140,'USD','TJS',11.404129,'2021-06-08 18:21:01'),(141,'USD','TMT',3.51,'2021-06-05 02:27:58'),(142,'USD','TND',2.726497,'2021-06-08 18:21:01'),(143,'USD','TOP',2.23145,'2021-06-05 02:27:58'),(144,'USD','TRY',8.613597,'2021-06-08 18:21:01'),(145,'USD','TTD',6.78273,'2021-06-08 18:21:01'),(146,'USD','TWD',27.7005,'2021-06-08 18:21:01'),(147,'USD','TZS',2318.999768,'2021-06-08 18:21:01'),(148,'USD','UAH',27.175372,'2021-06-08 18:21:01'),(149,'USD','UGX',3537.685797,'2021-06-08 18:21:01'),(150,'USD','USD',1,'2021-06-04 05:18:13'),(151,'USD','UYU',43.590988,'2021-06-08 18:21:01'),(152,'USD','UZS',10547.000288,'2021-06-08 18:21:01'),(153,'USD','VEF',213830222338.07,'2021-06-04 05:18:13'),(154,'USD','VND',22955.5,'2021-06-08 18:21:01'),(155,'USD','VUV',108.407216,'2021-06-04 05:18:13'),(156,'USD','WST',2.51595,'2021-06-04 05:18:13'),(157,'USD','XAF',539.562564,'2021-06-08 18:21:01'),(158,'USD','XAG',0.035917,'2021-06-08 18:21:01'),(159,'USD','XAU',0.000527,'2021-06-08 18:21:01'),(160,'USD','XCD',2.70255,'2021-06-04 05:18:13'),(161,'USD','XDR',0.694979,'2021-06-08 18:21:01'),(162,'USD','XOF',541.498708,'2021-06-08 18:21:01'),(163,'USD','XPF',98.450127,'2021-06-08 18:21:01'),(164,'USD','YER',250.049784,'2021-06-08 18:21:01'),(165,'USD','ZAR',13.53317,'2021-06-08 18:21:01'),(166,'USD','ZMK',9001.200677,'2021-06-08 18:21:01'),(167,'USD','ZMW',22.543041,'2021-06-08 18:21:01'),(168,'USD','ZWL',322.000305,'2021-06-04 05:18:13');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fav_curr`
--

DROP TABLE IF EXISTS `fav_curr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fav_curr` (
  `id` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `src` varchar(3) CHARACTER SET ascii NOT NULL DEFAULT 'USD',
  `dst` varchar(3) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pair` (`src`,`dst`,`user_id`) USING BTREE,
  KEY `user_id` (`user_id`),
  KEY `curr_ibfk_1` (`dst`),
  CONSTRAINT `curr_ibfk_1` FOREIGN KEY (`dst`) REFERENCES `currencies` (`dst`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fav_curr`
--

LOCK TABLES `fav_curr` WRITE;
/*!40000 ALTER TABLE `fav_curr` DISABLE KEYS */;
INSERT INTO `fav_curr` VALUES (4,513348783,'USD','CNY'),(1,513348783,'USD','EUR'),(2,513348783,'USD','GBP'),(5,513348783,'USD','JPY'),(3,513348783,'USD','RUB');
/*!40000 ALTER TABLE `fav_curr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `index_view`
--

DROP TABLE IF EXISTS `index_view`;
/*!50001 DROP VIEW IF EXISTS `index_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `index_view` (
  `id` tinyint NOT NULL,
  `user_id` tinyint NOT NULL,
  `date_time` tinyint NOT NULL,
  `Bot` tinyint NOT NULL,
  `username` tinyint NOT NULL,
  `command` tinyint NOT NULL,
  `iscommand` tinyint NOT NULL,
  `answere` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `message_type`
--

DROP TABLE IF EXISTS `message_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `action` (`action`)
) ENGINE=InnoDB AUTO_INCREMENT=452 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_type`
--

LOCK TABLES `message_type` WRITE;
/*!40000 ALTER TABLE `message_type` DISABLE KEYS */;
INSERT INTO `message_type` VALUES (435,'* 12:00'),(433,'/addfav \nCNY JPY INR'),(432,'/addfav'),(440,'/addsub'),(438,'/all'),(437,'/delfav \nINR'),(436,'/delfav'),(434,'/delsub'),(431,'/fav'),(428,'/help'),(446,'/history \n2021/06/09 - 2021/06/10'),(445,'/history'),(430,'/show \nCNY JPY INR'),(429,'/show'),(427,'/start'),(439,'/sub'),(451,'/subs'),(447,'/котик'),(449,'?'),(441,'w 9:05'),(443,'w 9:10, 9:15 / thu 9:10'),(442,'w 9:10, thu 9:10'),(444,'wd 10:00'),(450,'we 12:20'),(448,'кек');
/*!40000 ALTER TABLE `message_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subs_history`
--

DROP TABLE IF EXISTS `subs_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subs_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sub_id` int(11) unsigned NOT NULL,
  `answer_id` int(11) unsigned NOT NULL,
  `finished` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `subs_history_ibfk_2` (`answer_id`),
  KEY `subs_history_ibfk_1` (`sub_id`),
  CONSTRAINT `subs_history_ibfk_1` FOREIGN KEY (`sub_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `subs_history_ibfk_2` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subs_history`
--

LOCK TABLES `subs_history` WRITE;
/*!40000 ALTER TABLE `subs_history` DISABLE KEYS */;
INSERT INTO `subs_history` VALUES (1,235,327,'2021-06-09 09:15:00');
/*!40000 ALTER TABLE `subs_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `weekdays` tinyint(3) unsigned NOT NULL,
  `time` time NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`time`),
  CONSTRAINT `subscription_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
INSERT INTO `subscription` VALUES (231,513348783,4,'09:05:00',1),(234,513348783,12,'09:10:00',1),(235,513348783,4,'09:15:00',1),(236,513348783,31,'10:00:00',1),(237,513348783,96,'12:20:00',1);
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `talk`
--

DROP TABLE IF EXISTS `talk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `talk` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `bot_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `chat_id` int(11) unsigned NOT NULL DEFAULT 0,
  `message_type_id` int(11) unsigned NOT NULL,
  `answer_id` int(11) unsigned NOT NULL,
  `update_id` int(11) unsigned NOT NULL,
  `success` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `iscommand` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `talk_ibfk_1` (`user_id`),
  KEY `talk_ibfk_2` (`bot_id`),
  KEY `talk_ibfk_3` (`message_type_id`),
  KEY `talk_ibfk_4` (`answer_id`),
  CONSTRAINT `talk_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `talk_ibfk_2` FOREIGN KEY (`bot_id`) REFERENCES `bot` (`id`),
  CONSTRAINT `talk_ibfk_3` FOREIGN KEY (`message_type_id`) REFERENCES `message_type` (`id`),
  CONSTRAINT `talk_ibfk_4` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `talk`
--

LOCK TABLES `talk` WRITE;
/*!40000 ALTER TABLE `talk` DISABLE KEYS */;
INSERT INTO `talk` VALUES (1,'2021-06-09 06:00:20',1,513348783,513348783,427,319,755413732,1,1),(2,'2021-06-09 06:01:03',1,513348783,513348783,428,320,755413733,1,1),(3,'2021-06-09 06:01:06',1,513348783,513348783,429,321,755413734,1,0),(4,'2021-06-09 06:01:14',1,513348783,513348783,430,322,755413735,1,1),(5,'2021-06-09 06:01:19',1,513348783,513348783,431,323,755413736,1,1),(6,'2021-06-09 06:01:24',1,513348783,513348783,432,321,755413737,1,0),(7,'2021-06-09 06:01:30',1,513348783,513348783,433,324,755413738,1,1),(8,'2021-06-09 06:01:35',1,513348783,513348783,434,325,755413739,1,1),(9,'2021-06-09 06:01:50',1,513348783,513348783,435,326,755413740,1,0),(10,'2021-06-09 06:01:58',1,513348783,513348783,428,320,755413741,1,1),(11,'2021-06-09 06:02:02',1,513348783,513348783,436,321,755413742,1,0),(12,'2021-06-09 06:02:08',1,513348783,513348783,437,327,755413743,1,1),(13,'2021-06-09 06:02:14',1,513348783,513348783,438,328,755413744,1,1),(14,'2021-06-09 06:02:21',1,513348783,513348783,439,329,755413745,1,1),(15,'2021-06-09 06:02:31',1,513348783,513348783,440,325,755413746,1,1),(16,'2021-06-09 06:03:19',1,513348783,513348783,441,330,755413747,1,0),(17,'2021-06-09 06:03:32',1,513348783,513348783,440,325,755413748,1,1),(18,'2021-06-09 06:03:58',1,513348783,513348783,442,331,755413749,1,0),(19,'2021-06-09 06:04:10',1,513348783,513348783,440,325,755413750,1,1),(20,'2021-06-09 06:04:28',1,513348783,513348783,443,332,755413751,1,0),(21,'2021-06-09 06:04:38',1,513348783,513348783,440,325,755413752,1,1),(22,'2021-06-09 06:04:51',1,513348783,513348783,444,333,755413753,1,0),(23,'2021-06-09 06:05:07',1,513348783,513348783,428,320,755413754,1,1),(24,'2021-06-09 06:05:19',1,513348783,513348783,445,334,755413755,1,0),(25,'2021-06-09 06:05:38',1,513348783,513348783,446,335,755413756,1,1),(26,'2021-06-09 06:06:17',1,513348783,513348783,431,327,755413757,1,1),(27,'2021-06-09 06:06:34',1,513348783,513348783,447,336,755413758,1,1),(28,'2021-06-09 06:06:39',1,513348783,513348783,448,331,755413759,1,0),(29,'2021-06-09 06:06:48',1,513348783,513348783,449,331,755413760,1,0),(30,'2021-06-09 06:09:42',1,513348783,513348783,440,325,755413761,1,1),(31,'2021-06-09 06:10:03',1,513348783,513348783,450,337,755413762,1,0),(32,'2021-06-09 06:10:49',1,513348783,513348783,451,331,755413763,1,0),(33,'2021-06-09 06:10:59',1,513348783,513348783,439,338,755413764,1,1);
/*!40000 ALTER TABLE `talk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL,
  `username` varchar(64) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `language_code` varchar(2) CHARACTER SET ascii NOT NULL,
  `is_bot` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (513348783,'madcored','m҉a҉d҉c҉o҉r҉e҉','ru',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_routes`
--

DROP TABLE IF EXISTS `user_routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_routes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `chat_id` int(11) unsigned NOT NULL,
  `from` varchar(64) CHARACTER SET ascii NOT NULL DEFAULT '''''',
  `next` varchar(64) CHARACTER SET ascii NOT NULL DEFAULT '''''',
  `add_time` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_routes`
--

LOCK TABLES `user_routes` WRITE;
/*!40000 ALTER TABLE `user_routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `index_view`
--

/*!50001 DROP TABLE IF EXISTS `index_view`*/;
/*!50001 DROP VIEW IF EXISTS `index_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=MERGE */
/*!50013 DEFINER=`madcore`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `index_view` AS select `talk`.`id` AS `id`,`user`.`id` AS `user_id`,`talk`.`date_time` AS `date_time`,`bot`.`name_bot` AS `Bot`,`user`.`username` AS `username`,`message_type`.`action` AS `command`,`talk`.`iscommand` AS `iscommand`,`answer`.`answer` AS `answere` from ((((`talk` join `message_type` on(`talk`.`message_type_id` = `message_type`.`id`)) join `bot` on(`talk`.`bot_id` = `bot`.`id`)) join `user` on(`talk`.`user_id` = `user`.`id`)) join `answer` on(`talk`.`answer_id` = `answer`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-09  9:15:42
