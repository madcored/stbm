<?php
namespace app\controllers;

error_reporting(E_ALL);

use Yii;
use app\models\Currencies;
use app\models\FavCurr;
use app\models\FavCurrSearch;
use app\models\IndexView;
use app\models\Subscription;

class Zoltor_currController extends Def_telebotController
{
    protected function commStart()
    {
        return " <b>Welcome, " . $this->message['from']['first_name'] 
            . "</b> \n I`m just a currency bot and know nothing.\n"
            ." Loan /help for a variety.\n";
    }

    private $commands = [
        'start'     => "For start", 
        'help'      => "For help", 
        'котик'     => "котик\n\n<b>Currencies info</b>", 
        'show'      => "View currencies",
        'fav'       => "Show your favorit currencies", 
        'addfav'    => "Add currencies to your favorites", 
        'delfav'    => "Remove currencies from your favorites", 
        'all'       => "View all currencies\n\n<b>subscriptions</b>",
        'sub'       => "View your scheduled subscriptions",
        'addsub'    => "Add your favs to scheduled subscriptions",
        'delsub'    => "Remove from scheduled subscriptions\n\n<b>History</b>",
        'history'   => "View your history for date inretval",
        'subhistory'   => "View your sheduled task history",
    ];

    protected function commHelp()
    {   
        $help = "You can control me by sending these commands:\n\n";
        foreach ($this->commands as $key => $val) $help .= " /$key \t - $val \n";
        return $help;
    }

    protected function commCurrencies($fav = [])
    {
        (new \app\models\Currencies)->updateCurrencies();
        $resp = '';
        if (count($fav) > 0)
        {
            $in = [];
            foreach ($fav as $val) $in[] = $val['dst'];
            if(!($curr = Currencies::find()->where(['in', 'dst', $in])->asArray()->all())) return "nothing\n";
            foreach ($curr as $val) $resp .= "${val['src']} / ${val['dst']} : \t${val['value']} \n";
        } else 
        {
            if(!($curr = Currencies::find()->limit(255)->asArray()->all())) return "nothing\n";
            foreach ($curr as $val) $resp .= "${val['src']} / ${val['dst']} : \t${val['value']} \n";
        }
        return $resp;
    }
    
    protected function commAll()
    {
        return $this->commCurrencies();
    }   

    protected  function commкотик()
    {
        $this->telegram->sendPhoto([
            'chat_id' => $this->talk->chat_id, 
            'photo' => 'http://placekitten.com/' . random_int(400,600) . '/' . random_int(400,600) . '?a=' . random_int(1,100000),
        ]);
        return "котик";
    }   

    protected $favs = [];
    
    protected function initFavs()
    {
        if (!count($this->favs) and Yii::$app->db
            ->createCommand('SELECT COUNT(*) FROM fav_curr WHERE user_id =' . $this->talk->user_id)
            ->queryScalar() == 0
        )
            \Yii::$app->db->createCommand(
                \Yii::$app->db->queryBuilder->batchInsert('fav_curr', ['user_id', 'dst'], [
                [$this->talk->user_id, 'EUR'], 
                [$this->talk->user_id, 'GBP'], 
                [$this->talk->user_id, 'RUB']            
                ]))->execute();
        $this->favs = (new \yii\db\Query())
            ->select(['src', 'dst'])
            ->from('fav_curr')
            ->where(['user_id' => $this->talk->user_id])
            ->limit(256)
            ->all();
    }

    protected function commFav()
    {
        $this->initFavs();
        return $this->commCurrencies($this->favs);
    }   

    function checkRoutes() {
        if ($routes = Yii::$app->db
            ->createCommand("SELECT * FROM `user_routes` ")->queryAll()
        ){
            foreach ($routes as $r) {
                $method = $r['next'];
                if ( method_exists($this, $method )) 
                    if($res = $this->$method()) $this->answer = $res;
            }
            return true;
        }
    }

    public function actionHook()
    {   
        if(!isset($this->message['text'])) return;
        if ($this->checkRoutes()) return;
        
        $method = 'comm' . ltrim($this->message['text'], '/');
        if($this->message['text'][0] == '/' and method_exists($this, $method))
        {
            $this->talk->iscommand = 1;
            $this->answer = $this->$method();
        } 
        else $this->answer = "You cute, " . $this->message['from']['first_name'] . "\n Try /help for more info";
    }

    protected function commSub()
    {   
        $resp = '';
        if($subs = Yii::$app->db
            ->createCommand("SELECT * FROM `subscription` AS sub "
                . " INNER JOIN user ON user.id = sub.user_id "
                . " WHERE sub.active = 1 "
                . " AND user.id = sub.user_id "
                . " AND sub.weekdays <> 0 "
                . " ORDER BY weekdays, `time` "
                . " LIMIT 30 ")
            ->queryAll()
        )
        {
            $preday = 128;
            foreach ($subs as $sub)
            {   
                if (!$sub['weekdays'] ) continue;
                if ( $sub['weekdays'] != $preday )
                {
                    $resp .= "\n<b> " . \app\models\Subscription::wdaysToString($sub['weekdays']) . ":</b>\n";
                    $preday = $sub['weekdays'];
                }
                $resp .= "{$sub['time']}\n";                
            }
            return $resp;
        } else return "nothing";
    }

    protected function regNext(string $next, int $user_id = 0, int $chat_id = 0, string $from = '')
    {
        $user_id or $user_id = $this->talk->user_id;
        $chat_id or $chat_id = $this->talk->chat_id;
        $sql = 'INSERT INTO user_routes SET '
            . " `user_id` = $user_id, `chat_id` = $chat_id, `next` = '$next' ";
        $from == '' or $sql .= ", `from` = '$from'";
        Yii::$app->db
            ->createCommand($sql)
            ->queryScalar();
    }
    
    protected function delNext(string $next = '', string $from = '', int $user_id = 0, int $chat_id = 0)
    {
        $sql = 'DELETE FROM user_routes WHERE '
            . " user_id = {$this->talk->user_id} AND chat_id = {$this->talk->user_id} ";
        Yii::$app->db
            ->createCommand($sql)
            ->queryScalar();
    }
    
    function mapped_implode($glue, $array, $symbol = '/') {
        return implode($glue, array_map(
                function($k, $v) use($symbol) {
                    return $k . $symbol . $v;
                },
                array_keys($array),
                array_values($array)
                )
            );
    }
    
    function inputSubData(string $s) 
    {
        if ($s[0] == '/' 
            or !(preg_match("/^[a-zA-Z0-9\/\*\s\t\:,]+$/", $s))
            or !($subtime = \app\models\Subscription::stringToWdays($s))
        ){
            $this->answer = "Enter days of week with time, for example:\n"
                . "mon 11:00 / fri 10:00, 17:50 / * 12:45 / workdays 10:00,12:00, 14:00, 16:00";
            $this->talk->iscommand = 0;
            return false;
        }
        return " ({$this->talk->user_id}, '" . $this->mapped_implode('), (' . $this->talk->user_id . ', \'', $subtime, '\', ') . ') ';
    }
    
    function insSubData(string $sql)
    {
        $this->delNext();
        Yii::$app->db
            ->createCommand($sql)
            ->queryScalar();
        return "<b>Your sheduled subscriptions: </b>\n" . $this->commSub();
    }
    
    protected function CommAddsub()
    {
        $this->message['text'] != '/addsub' or $this->regNext('commAddsub');
        if (!($dataStr = $this->inputSubData($this->message['text']))) return $this->answer;
        $sql = 'INSERT INTO subscription ( `user_id`, `time`, `weekdays`) VALUES '
            . $dataStr
            . 'ON DUPLICATE KEY UPDATE weekdays = weekdays | VALUES(weekdays)';
        return $this->insSubData($sql);
    }

    protected function CommDelsub()
    {
        $this->message['text'] != '/delsub' or $this->regNext('commDelsub');
        if (!($dataStr = $this->inputSubData($this->message['text']))) return $this->answer;
        $sql = 'INSERT INTO `subscription` ( `user_id`, `time`, `weekdays`) VALUES '
            . $dataStr
            . 'ON DUPLICATE KEY UPDATE weekdays = `weekdays` & (VALUES(`weekdays`) ^ 127)';
        $res = $this->insSubData($sql);
        Yii::$app->db
            ->createCommand($sql)
            ->queryScalar();
        $sql = "DELETE FROM `subscription` "
            . " WHERE `user_id` = {$this->talk->user_id} "
            . " AND (`weekdays` = 0 "
            . " OR (( `user_id`, `time`, `weekdays`) IN ($dataStr))) ";
        $res = $this->insSubData($sql);
        return $res;
    }

    function validateDate($date, $format = 'Y/m/d') {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }    

    private function commHistory()
    {
        $this->regNext('History');
        return $this->History();
    }
        
    public function History()
    {
        $commForhistory = " 'fav|addfav|delfav|show|all' ";
        $this->telegram->sendMessage([
                    'chat_id' => $this->message['chat']['id'],
                    'text' => __FUNCTION__ . " recieved: " . $this->message['text'] . "  \n",
        ]);      
        $this->talk->iscommand = 0;
        $startd = '(NOW() - INTERVAL 1 DAY)';
        $endd = 'NOW()';
        $dd = explode('-', $this->message['text']);
        foreach($dd as &$val) $val = trim($val);
        if ($this->message['text'][0] != '/'
            and (count($dd) == 2 and $this->validateDate($dd[0]) and $this->validateDate($dd[1])))
        {
            $startd = "'{$dd[0]}'";
            $endd = "'{$dd[1]}'";
        } else return "Enter date interval 'Y/m/d - Y/m/d', for example:\n"
                        . "2021/06/09 - 2021/06/10";       
        
        $this->delNext();
        $resp = "";
        if($act = Yii::$app->db
            ->createCommand("SELECT user_id, date_time, command, answere FROM `index_view` "
                . " WHERE "
                . " `iscommand` = 1 "
                . " AND date_time BETWEEN $startd AND $endd "
                . " AND user_id = " . $this->talk->user_id
                . " AND command  regexp $commForhistory "
                . " LIMIT 1000 ")
            ->queryAll()
        )
        {
//             if ($pid = pcntl_fork()) {
            $i = 0;
            foreach ($act as $val)
            {
                $this->telegram->sendMessage([
                            'chat_id' => $this->talk->chat_id,
                            'parse_mode' => 'html',
                            'text' => "<b>Date: {$val['date_time']}</b>\n\n"
                                . "<b>Command:</b> \n{$val['command']}\n\n"
                                . "<b>Answer:</b> \n{$val['answere']}\n"
                ]);
                if (($i++)&4) sleep(1);
            }
            $resp .= $resp . "\n\n<b>DONE</b>";
//             }
        } 
        else $resp =  "nothing\n";
        $this->talk->iscommand = 1;            
        $this->message["text"] = "/history \n" . $this->message["text"];
        
        $this->answer = $resp;
    }

    function inputCurr()
    {
        if (!($this->message['text'])
            or !(preg_match("/^[a-zA-Z\s]+$/", $this->message['text']))
        ){
            $this->talk->iscommand = 0;
            return "Enter currencies to view, space separated, for example:\n"
                            . "CNY JPY INR";
        }
        $this->delNext();
        return false;
    }
    
    protected function commShow()
    {
        $this->message['text'] != '/show' or $this->regNext('commShow');
        if ($res = $this->inputCurr()) return $res;
        $newfavs = [];
        foreach (explode(' ', $this->message['text']) as $val) $newfavs[] = ['src' => 'USD', 'dst' => $val];
        $this->answer = $this->commCurrencies($newfavs);
        $this->talk->iscommand = 1;
        $this->message["text"] = "/show \n" . $this->message["text"];
    }
    
    protected function commAddfav()
    {   
        if ($this->message['text'] == '/addfav') $this->regNext('commAddfav');
        if ($res = $this->inputCurr()) return $res;
        $newfavs = str_replace('  ', ' ', $this->message['text']);
        $newfavs = "({$this->talk->user_id}, '" . str_replace(' ', "'), ({$this->talk->user_id}, '", $newfavs  ) . "')";
        $sql = 'INSERT IGNORE INTO fav_curr (user_id, dst) VALUES ' . $newfavs;
        Yii::$app->db
            ->createCommand($sql)
            ->queryScalar();
        $this->initFavs();
        $this->talk->iscommand = 1;
        $this->message["text"] = "/addfav \n" . $this->message["text"];
        return $this->commFav();
    }
                    
    protected function commDelfav()
    {   
        if ($this->message['text'] == '/delfav') $this->regNext('commDelfav');
        if ($res = $this->inputCurr()) return $res;
        $newfavs = str_replace('  ', ' ', $this->message['text']);
        $newfavs = str_replace(' ', "', '", $newfavs);
        $sql = "DELETE FROM `fav_curr` WHERE dst IN ( '$newfavs' ) AND user_id = {$this->talk->user_id}";
        Yii::$app->db
            ->createCommand($sql)
            ->queryScalar();
        $this->initFavs();
        $this->talk->iscommand = 1;
        $this->message["text"] = "/delfav \n" . $this->message["text"];
        return $this->commFav();
    }

    public function actionSheduler()
    {
        $weekday = 1 << (date('N', time()) - 1);
        if($subs = Yii::$app->db
            ->createCommand("SELECT sub.id AS sub_id, sub.user_id "
                . " , cast(concat(DATE(NOW()), ' ', `sub`.`time`) as datetime) as dt, sh.finished, NOW() "
                . " FROM `subscription` AS sub "
                . " LEFT JOIN subs_history AS sh ON sub.id = sh.sub_id "
                . " WHERE sub.active = 1 "
                . " AND sh.id IS NULL "
                . " AND sub.weekdays & $weekday <> 0 "
                . " AND  `time` BETWEEN NOW() - INTERVAL 45 SECOND and NOW()"
                . " AND (sh.finished >= cast(concat(DATE(NOW()), ' ', `sub`.`time`) as datetime)  OR sh.finished IS NULL) "
                . " ORDER BY `time` ")
            ->queryAll()) 
        {
            foreach ($subs as $sub) {
                print_r($sub);
                $this->talk->user_id = $sub['user_id'];
                $answer = $this->commFav();
                $this->talk->answer_id = $this->initModel('\app\models\Answer', ['answer' => $answer], ['answer' => $answer])->id;
                Yii::$app->db
                    ->createCommand(
                        "INSERT INTO subs_history SET sub_id = {$sub['sub_id']}, "
                        . "answer_id = {$this->talk->answer_id}"
                    )->queryScalar();
                echo date("H:i:s") . "\n\n";
                $this->telegram->sendMessage([
                    'chat_id' => $this->talk->user_id, 
                    'parse_mode' => 'html',
                    'text' => "<b> Your subscription: </b>\n" . $this->commFav(),
                ]);
            }
        }
    }

}
