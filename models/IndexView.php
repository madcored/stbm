<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "index_view".
 *
 * @property int $id
 * @property string $date_time
 * @property string $Bot
 * @property string $username
 * @property string $command
 * @property int $iscommand
 * @property string $answere
 */
class IndexView extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'index_view';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'iscommand'], 'integer'],
            [['date_time'], 'safe'],
            [['Bot', 'username', 'command', 'answere'], 'required'],
            [['answere'], 'string'],
            [['Bot', 'username'], 'string', 'max' => 64],
            [['command'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'date_time' => 'Date Time',
            'Bot' => 'Bot',
            'username' => 'Username',
            'command' => 'Command',
            'iscommand' => 'Iscommand',
            'answere' => 'Answere',
        ];
    }

    public static function primaryKey()
    {
            return ['id'];
    }

}
