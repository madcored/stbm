<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubscriptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subscriptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Subscription', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            [
            'label' => 'TeleUser',
            'attribute' => 'user_id',
            'value' => fn($data) => \app\models\User::find()->where(['id' => $data->user_id ])->one()->username,

            ],
            [
                'label' => 'Days',
                'attribute' => 'weekdays',
                'value' => fn($data) => \app\models\Subscription::wdaysToString($data->weekdays),
            ],
            'time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
