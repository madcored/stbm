<?php

namespace app\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Currencies;
use app\models\CurrenciesSearch;

class CurrenciesController extends \yii\web\Controller
{
    protected CONST ACCESS_KEY = '7012b044a50468e63688b28de376a79a';
    protected CONST UPDATE_PERIOD = '-1 hour';

    public function actionIndex()
    {
        (new \app\models\Currencies)->updateCurrencies();
        $searchModel = new CurrenciesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
