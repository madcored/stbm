<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currencies".
 *
 * @property int $id
 * @property string $src
 * @property string $dst
 * @property float $value
 * @property string $updated
 */
class Currencies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currencies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dst', 'value'], 'required'],
            [['value'], 'number'],
            [['updated'], 'safe'],
            [['src', 'dst'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'src' => 'Src',
            'dst' => 'Dst',
            'value' => 'Value',
            'updated' => 'Updated',
        ];
    }

    protected CONST ACCESS_KEY = '7012b044a50468e63688b28de376a79a';
    protected CONST UPDATE_PERIOD = '-1 hour';

    function updateCurrencies($types = [])
    {
        if ($model = self::find()->where(['dst' => 'RUB'])->one()
            and (strtotime($model->updated) < strtotime(self::UPDATE_PERIOD))
        ) return true;
        error_log("\n\n!                trying update CURR!!!    !\n\n!");
        $client = new \yii\httpclient\Client();
        $response = $client->get('http://api.currencylayer.com/live?access_key=' . self::ACCESS_KEY)->send();
        if (!($response->isOk)) return false;
        error_log("\n\n!                trying update CURR: return isOk    !\n\n!");
        $resp_curr = \yii\helpers\BaseJson::decode($response->getContent())['quotes'];
        foreach($resp_curr as $key => $val) {
            $src = substr($key, 0, 3);
            $dst = substr($key, 3, 3);
            $curr[] = [$src, $dst, $val];
            if (!($model = Currencies::find()->where(['src'=> $src, 'dst' => $dst])->one()))
            {
                $model = new Currencies;
                $model->src = $src;
                $model->dst = $dst;
                $model->value = $val;
                $model->save();            
                return true;
            }
            $model->value = $val;
            $model->save();            
        }
        return true;
    }
    
//    public function behaviors()
//    {
//        return[
//             \kozhemin\dbHelper\InsertUpdate::className(),
//        ];
//    }
   
}
