<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IndexView */

$this->title = 'Create Index View';
$this->params['breadcrumbs'][] = ['label' => 'Index Views', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="index-view-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
