<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bot-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bot', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'Bot',
                'attribute' => 'name_bot',
                'value' => fn($data) => '@' . $data->name_bot,
            ],
            'name',
            'backend',
            'token',
            [
                'attribute' => 'active',
                'contentOptions' => ['style' => 'min-width:90px;text-align:center;'],
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'active',
                    [null => '---', 1 => 'Yes', 0 => 'No'],
                    ['class' => 'form-control']),
                    'value' => fn($data) =>  var_export((boolean)$data->active, true),
            ],
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
