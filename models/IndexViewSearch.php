<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IndexView;

/**
 * IndexViewSearch represents the model behind the search form of `app\models\IndexView`.
 */
class IndexViewSearch extends IndexView
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'iscommand'], 'integer'],
            [['date_time', 'Bot', 'username', 'command', 'answere'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IndexView::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_time' => $this->date_time,
            'iscommand' => $this->iscommand,
        ]);

        $query->andFilterWhere(['like', 'Bot', $this->Bot])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'command', $this->command])
            ->andFilterWhere(['like', 'answere', $this->answere]);

        return $dataProvider;
    }
}
