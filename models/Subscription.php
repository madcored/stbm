<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property int $id
 * @property int $user_id
 * @property int $weekdays
 * @property string $time
 *
 * @property User $user
 */
class Subscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'weekdays', 'time'], 'required'],
            [['id', 'user_id', 'weekdays'], 'integer'],
            [['time'], 'safe'],
            [['user_id', 'weekdays', 'time'], 'unique', 'targetAttribute' => ['user_id', 'weekdays', 'time']],
            [['id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'weekdays' => 'Weekdays',
            'time' => 'Time',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    static function wdaysToString(int $d = 0)
    {
        static $days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        switch ($d)
        {
            case 127:
                return 'everyday';
            case 31:
                return 'work days(mon - fri)';
            case 96:
                return 'weekends(sat - sun)';
            default:
                $subdays = '';
                $i = 0;
                while ($d)
                {
                    if ($d & 1) $subdays .= ' ' . $days[$i]; 
                    $i++;
                    $d >>= 1;
                }
                return ltrim($subdays);
        }
    }
    
    static function stringToWdays(string $str = '', int $z = 0)
    {
        static $daysnum = ['mon' => 1, 'm' => 1, 'tue' => 2, 'wed' => 4, 'w' => 4,
            'thu' => 8, 'fri' => 16, 'f' => 16, 'sat' => 32, 'sun' => 64, 
            'everyday' => 127, '*' => 127, 'workdays' => 31, 'wd' =>31,
            'weekends' => 96, 'we' => 96];
        $subtime = [];
        foreach (explode('/', $str) as $val )
        {
            if (count($wd = explode(' ', trim($val), 2)) !=2 
                or !array_key_exists($wd[0] = trim($wd[0]), $daysnum)) continue;
            $days = 0;
            $z or $days = $daysnum[$wd[0]];
            foreach(explode(',', trim($wd[1])) as $t)
            {
                $t = trim($t);
                if(!strtotime($t)) continue;
                array_key_exists($t, $subtime) or $subtime[$t] = 0;
                $subtime[$t] |= $days;
            }
        }
        return $subtime;
    }  
    
}
