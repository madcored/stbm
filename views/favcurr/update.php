<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FavCurr */

$this->title = 'Update Fav Curr: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fav Currs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fav-curr-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
